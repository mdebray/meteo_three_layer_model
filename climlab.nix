{ lib , buildPythonPackage , fetchFromGitHub , numpy, scipy, future, pooch, xarray, gfortran }:
buildPythonPackage rec {
  pname = "climlab";
  version = "0.7.13";
  src = fetchFromGitHub {
    owner = "brian-rose";
    repo = "climlab";
    rev="v0.7.13";
    sha256 = "1l5afn9zvr88x8lkfpiq8vw66fjv2gpz61m1k410hh9z330y5vkm";
  };
  doCheck = false;
  nativeBuildInputs = [ gfortran numpy scipy pooch xarray ];
  propagatedBuildInputs = [ numpy scipy pooch xarray ];
  meta = with lib; {
    description = "Python package for process-oriented climate modeling";
    homepage = "https://github.com/brian-rose/climlab";
    license = licenses.mit;
  };
}
