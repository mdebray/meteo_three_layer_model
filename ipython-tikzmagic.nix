{ lib , buildPythonPackage , fetchFromGitHub , ipython }:
buildPythonPackage rec {
  pname = "tikzmagic";
  version = "1";
  src = fetchFromGitHub {
    owner = "mkrphys";
    repo = "ipython-tikzmagic";
    rev="f9357cc86ea5848947f3d189ee736031bb64890d";
    sha256 = "1500ijn1y871v4flgvjgz3hnf520nlk5h3lss5dsbc5rc5dqkprw";
  };
  doCheck = false;
  propagatedBuildInputs = [ ipython ];
  meta = with lib; {
    description = "IPython magics for generating figures with TikZ";
    homepage = "https://github.com/mkrphys/ipython-tikzmagic";
    license = licenses.bsd3;
  };
}
