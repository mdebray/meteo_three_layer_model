with import <nixpkgs> {};
let
  climlab = python39Packages.callPackage ./climlab.nix { inherit gfortran; };
  ipythonTikzmagic = python39Packages.callPackage ./ipython-tikzmagic.nix { };
in mkShell {
  packages = [
    (python39.withPackages (ps: [
      climlab
      ipythonTikzmagic
      ps.numpy
      ps.sympy
      ps.scipy
      ps.jupyterlab
      ps.matplotlib
    ]))
    pdf2svg
    pandoc
    texlive.combined.scheme-full
    git
    inkscape
  ];
}
